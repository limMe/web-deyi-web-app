﻿//Basic Support for all self written js
MAX_TIMELAPSE = 60*5;
NONCE_TOKEN = "000000";
API_PATH = "http://123.56.144.82/WebApi/";

//API_PATH = "localhost:24614/WebApi/";

function push_alert(str){
	$("body").before("<div class=\"alert alert-warning alert-dismissable\">" + "</div>");
	$(".alert").last().append("<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" + str);
}

function push_danger(str){
	$("body").before("<div class=\"alert alert-danger  alert-dismissable\">" + "</div>");
	$(".alert").last().append("<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" + str);
}

function hideAllPanel(){
    var panels = document.getElementsByClassName("Panel");
    for(var i=0; i<panels.length; i++){
        panels[i].style.display = "none";
    }
}

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}

function toDisplayMoney(money100){
    var head = Math.floor(money100/100);
    var tail = money100%100;
    return head.toString()+"."+tail.toString()+"元";
}

function checkTick() {
	getNonceToken();
    var signTick = getQueryString("tick");
    var currentTick = Math.round(new Date().getTime()/1000);
	
    if(getQueryString("isViewOnly") == true){
        return true;
    }
    else{
        //C.C 注意和后台返回的tick计算方法要一致
        if( currentTick - signTick > MAX_TIMELAPSE
		  ||signTick==null){
            window.location.href = "noauth.html";
            return false;
        }
        return true;
    }
}

//其实任何人开发者都能通过这个拿到token
//这里做不了安全，能做的只有防用户误操作
function getNonceToken(){
    xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.onreadystatechange = function(){
        if(xmlHttpRequest.readyState == 4 && xmlHttpRequest.status == 200){    
            var gotJson = JSON.parse(xmlHttpRequest.responseText);
            NONCE_TOKEN = gotJson.data;
        }  
    };
    xmlHttpRequest.open("GET",API_PATH+"getNonceToken.aspx",true);
    xmlHttpRequest.send();
}