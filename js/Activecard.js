﻿FUNC_ACTIVECARD = "FUNC_ACTIVECARD";

$(document).ready(function(){
	if(checkTick()==true){
		var funcStr = getQueryString("func");
		if(funcStr==FUNC_ACTIVECARD){
			$("#activePanel").css("display","block");
		}else{
			$("#warningInfo").html("您所希望使用的功能正在开发中！");
        	$("#warningPanel").css("display","block");
		  }
	}
});

function active(){
		getNonceToken();
		var openid = getQueryString("openid");
		var cardId = $("input[id=cardId]").val();		
		var activecode = $("input[id=activecode]").val();
		if ( isNaN(cardId) || isNaN(activecode) ) {
				salerId = null;
				activecode = null;
				push_alert("请仅输入数字");
		}
			else
		$.ajax({
				url:API_PATH + "activeCard.aspx?openid=" + openid + "&nonceToken=" + NONCE_TOKEN + "&cardId=" + cardId + "&activecode=" + activecode,
				type:"get",
				timeout:5000, 
				success:function(data){
					var gotJson = JSON.parse(data);
					console.log(gotJson);
					if (gotJson.status == false){
						push_alert("查询记录失败，因为" + gotJson.errorMsg);
						$("#runningInfo").css("display","none");
						$("#activePanel").css("display","block");
						return ;
					}
					$("#runningInfo").css("display","none");
					$("#successInfo").css("display","block");
				},
				error:function(data){
					var gotJson = JSON.parse(data);
					console.log(gotJson);
					$("#runningInfo").css("display","none");
					$("#activePanel").css("display","block");
					push_danger(gotJson.errorMsg);
				},
				beforeSend:function(){
					$("#activePanel").css("display","none");
					$("#runningInfo").css("display","block");
				}
		});
}