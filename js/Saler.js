FUNC_QUERY_SALER_RECORDS = "FUNC_QUERY_SALER_RECORDS";

function salerOnload(){
	if(checkTick()==true){
		var funcStr = getQueryString("func");
		if(funcStr == FUNC_QUERY_SALER_RECORDS){
			document.getElementById("querySalerRecordsPanel").style.display="block";
		}else{
			document.getElementById("warningInfo").innerHTML = "您所希望使用的功能正在开发中！";
        	document.getElementById("warningPanel").style.display = "block";
		}						
	}
}

function querySalerRecords(){
    
    var salerId = getQueryString("SalerId");
    var beginDate = document.getElementById("querySalerRecords_beginDate").value;
    var endDate = document.getElementById("querySalerRecords_endDate").value;
    
    if( parseInt(beginDate)>20501231 
       || parseInt(beginDate)<20000101
       || parseInt(endDate)>20501231
       || parseInt(endDate)<20010101
       || isNaN(beginDate)
	   || isNaN(endDate)){
        document.getElementById("warningInfo").innerHTML = "日期格式有误！";
        document.getElementById("warningPanel").style.display = "block";
        return false;
    }
    
	document.getElementById("querySalerRecords_beginDate").value = "";
	document.getElementById("querySalerRecords_endDate").value = "";
	
    xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.onreadystatechange = function(){
        if(xmlHttpRequest.readyState == 4 && xmlHttpRequest.status == 200){    
            var gotJson = JSON.parse(xmlHttpRequest.responseText);
            if(gotJson.status==false){
                //错误处理
                document.getElementById("warningInfo").innerHTML = "查询记录失败，因为："+gotJson.errorMsg;
                document.getElementById("warningPanel").style.display = "block";
                return false;
            }else{
                //除掉以前余留的问题
                hideAllPanel();
                document.getElementById("querySaleeRecordsResultTable").innerHTML = "";
                document.getElementById("querySalerRecordsResultPanel").style.display = "block";
                //填充资料
                for(var i=0;i<gotJson.recordsList.length;i++){
                    var newItem = document.createElement("tr");
                    newItem.innerHTML = '<th scope="row">'+(i+1).toString()+'</th><td>'+gotJson.recordsList[i].userCard+'</td><td>'+gotJson.recordsList[i].date+'</td><td>'+gotJson.recordsList[i].time+'</td><td>'+toDisplayMoney(gotJson.recordsList[i].amount)+'</td>';
                    document.getElementById("querySaleeRecordsResultTable").appendChild(newItem);
                }
            }
        }  
     };
     xmlHttpRequest.open("GET",API_PATH+"querySalerRecords.aspx?salerId="+salerId+"&nonceToken="+NONCE_TOKEN+"&beginDate="+beginDate+"&endDate="+endDate,true);
     xmlHttpRequest.send();
}