FUNC_ADD_SALER = "FUNC_ADD_SALER";
FUNC_QUERY_ALL_SALER = "FUNC_QUERY_ALL_SALER";

function adminOnload(){
	if(checkTick()==true){
		var funcStr = getQueryString("func");
		if(funcStr == FUNC_ADD_SALER){
			document.getElementById("addSalerPanel").style.display = "block";
		}
		else if(funcStr == FUNC_QUERY_ALL_SALER){
			queryAllSalers();
		}
		else{
			document.getElementById("warningInfo").innerHTML = "您所希望使用的功能正在开发中！";
        	document.getElementById("warningPanel").style.display = "block";
		}
	}
}

function addNewSaler(){
	var openId = getQueryString("openId");
	var salerName = document.getElementById("addSaler_salerName").value;
	var salerLocation = document.getElementById("addSaler_salerLocation").value;
	var salerType = document.getElementById("addSaler_salerType").value;
	var salerDistrict = document.getElementById("addSaler_salerDistrict").value;
	
	if(openId==null){
		document.getElementById("warningInfo").innerHTML = "当前未使用微信登陆！";
        document.getElementById("warningPanel").style.display = "block";
        return false;
	}
	
	if(salerName==null||salerLocation==null||salerType==null||salerDistrict==null){
		document.getElementById("warningInfo").innerHTML = "请补充必要信息！";
        document.getElementById("warningPanel").style.display = "block";
        return false;
	}
	
	if(isNaN(salerDistrict)||isNaN(salerType)){
		document.getElementById("warningInfo").innerHTML = "商户类型和商户区域必须为数字代码！";
        document.getElementById("warningPanel").style.display = "block";
        return false;
	}
	
	document.getElementById("addSaler_salerName").value = "";
	document.getElementById("addSaler_salerLocation").value = "";
	document.getElementById("addSaler_salerType").value = "";
	document.getElementById("addSaler_salerDistrict").value = "";
	
	xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.onreadystatechange = function(){
        if(xmlHttpRequest.readyState == 4 && xmlHttpRequest.status == 200){    
            var gotJson = JSON.parse(xmlHttpRequest.responseText);
            if(gotJson.status==false){
				document.getElementById("warningInfo").innerHTML = "添加商户失败，因为："+gotJson.errorMsg;
        		document.getElementById("warningPanel").style.display = "block";
        		return false;
			}
			else{
				hideAllPanel();

				queryAllSalers();

				document.getElementById("addSalerResultPanel").style.display = "block";
				document.getElementById("addSalerResultCode").innerHTML = gotJson.data;
			}
        }  
     };
     xmlHttpRequest.open("GET",API_PATH+"addNewSaler.aspx?openId="+openId+"&nonceToken="+NONCE_TOKEN+"&salerName="+salerName+"&salerLocation="+salerLocation+"&salerType="+salerType+"&salerDistrict="+salerDistrict,true);
     xmlHttpRequest.send();
}

function queryAllSalers(){
	var openId = getQueryString("openId");
    
    xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.onreadystatechange = function(){
        if(xmlHttpRequest.readyState == 4 && xmlHttpRequest.status == 200){    
            var gotJson = JSON.parse(xmlHttpRequest.responseText);
            if(gotJson.status==false){
                //错误处理
                document.getElementById("warningInfo").innerHTML = "查询记录失败，因为："+gotJson.errorMsg;
                document.getElementById("warningPanel").style.display = "block";
                return false;
            }else{
                //除掉以前余留的问题
                var isFromAdd = false;
                if(document.getElementById("addSalerResultPanel").style.display == "block")
                {
                	isFromAdd = true;
                }
                hideAllPanel();
                if(isFromAdd)
                {
                	document.getElementById("addSalerResultPanel").style.display = "block";
                }
                document.getElementById("queryAllSalerResultTable").innerHTML = "";
                document.getElementById("queryAllSalerResultPanel").style.display = "block";
                //填充资料
                for(var i=0;i<gotJson.data.length;i++){
                    var newItem = document.createElement("tr");
                    newItem.innerHTML = '<th scope="row">'+gotJson.data[i].saler_id.toString()+'</th><td>'+gotJson.data[i].saler_name+'</td><td>'+gotJson.data[i].saler_location+'</td><td>'+gotJson.data[i].saler_district+'</td><td>'+gotJson.data[i].saler_type+'</td><td>'+gotJson.data[i].saler_activate_code+'</td>';
                    document.getElementById("queryAllSalerResultTable").appendChild(newItem);
                }
            }
        }  
     };
     xmlHttpRequest.open("GET",API_PATH+"queryAllSalers.aspx?openId="+openId+"&nonceToken="+NONCE_TOKEN,true);
     xmlHttpRequest.send();
}
