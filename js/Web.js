﻿var saler_id = null;
var saler_activate_code = null;
var login_success = false;

function login(){
	saler_id = $("input[id=saler_id]").val();		
	saler_activate_code = $("input[id=saler_activate_code]").val();
	if ( saler_id == "" || saler_activate_code == "" || isNaN(saler_id) || isNaN(saler_activate_code) ) {
				saler_id = null;
				saler_activate_code = null;
				push_alert("请输入合法");
				return;
	}
	if ($("#saveState")[0].checked){
		$.cookie("saler_id", saler_id,{expires:1}); 
		$.cookie("saler_activate_code",saler_activate_code,{expires:1}); 
	}
	else
	{
		$.cookie('saler_id', '', { expires: -1 });
		$.cookie('saler_activate_code', '', { expires: -1 });
	}
	$.ajax({
				url:API_PATH + "loginSaler.aspx?saler_id=" + saler_id + "&saler_activate_code=" + saler_activate_code,
				type:"get",
				timeout:5000, 
				success:function(data){
					var gotJson = JSON.parse(data);
					console.log(gotJson);
					if (gotJson.status == false){
						push_alert("登陆记录失败，因为" + gotJson.errorMsg);
						return ;
					}
					$("#currentId").html(gotJson.saler_id);
					$("#currentSaler").html(gotJson.saler_name);
					$("#currentAddress").html(gotJson.saler_location);
					$("#logform").css("display","none");
					$("#logsuccess").css("display","block");
					login_success = true;
					$("#logState").html("已登陆");
				},
				error:function(data){
					var gotJson = JSON.parse(data);
					console.log(gotJson);
					push_danger(gotJson.errorMsg);
				}
		});
}

function logout(){
	$.cookie('saler_id', '', { expires: -1 });
	$.cookie('saler_activate_code', '', { expires: -1 });
	$("#logform").css("display","block");
	$("#logsuccess").css("display","none");
	$("input[id=saler_id]").val("");
	$("input[id=saler_activate_code]").val("");
	login_success = false;
	$("#logState").html("登陆");
}

function consume(){
	if (!login_success) {
		push_alert("请先登录");
		return;
	}
	var customer_id = $("input[id=customer_id]").val();		
	var customer_cost = $("input[id=customer_cost]").val();
	if ( customer_id == "" || customer_cost == "" || isNaN(customer_id) || isNaN(customer_cost) ) {
				customer_id = null;
				customer_cost = null;
				push_alert("请输入合法");
				return;
	}
	$.ajax({
				url:API_PATH + "addNewDeal.aspx?saler_id=" + saler_id + "&customer_id=" + customer_id + "&customer_cost=" + customer_cost,
				type:"get",
				timeout:5000, 
				success:function(data){
					var gotJson = JSON.parse(data);
					console.log(gotJson);
					if (gotJson.status == false){
						push_alert("录入失败，因为" + gotJson.errorMsg);
						return ;
					}
					push_success("录入成功");
				},
				error:function(data){
					var gotJson = JSON.parse(data);
					console.log(gotJson);
					push_danger(gotJson.errorMsg);
				}
		});
}


function querySalerRecords(){
	if (!login_success) {
		push_alert("请先登录");
		return;
	}    
    var beginDate = $("#querySalerRecords_beginDate").val();
    var endDate = $("#querySalerRecords_endDate").val();
    
	if ( beginDate == "" || endDate == "" || isNaN(beginDate) || isNaN(endDate) ) {
				beginDate = null;
				endDate = null;
				push_alert("请输入合法");
				return;
	}
	
    if( parseInt(beginDate)>20501231 
       || parseInt(beginDate)<20000101
       || parseInt(endDate)>20501231
       || parseInt(endDate)<20010101
       || isNaN(beginDate)
	   || isNaN(endDate)){
        push_alert("日期超限");
        return;
    }
    $("#querySalerRecords_beginDate").val("");
	$("#querySalerRecords_endDate").val("");
		
	getNonceToken();	
	
    $.ajax({
				url:API_PATH + "querySalerRecords.aspx?salerId=" + saler_id + "&NONCE_TOKEN=" + NONCE_TOKEN + "&beginDate=" + beginDate + "&endDate=" + endDate,
				type:"get",
				timeout:5000, 
				success:function(data){
					var gotJson = JSON.parse(data);
					console.log(gotJson);
					if (gotJson.status == false){
						push_alert("查询失败，因为" + gotJson.errorMsg);
						return ;
					}
					$("#querySaleeRecordsResultTable").html("");
					$("#querySalerRecordsResultPanel").css("display","block");
					for(var i=0;i<gotJson.recordsList.length;++i){
						$("#querySaleeRecordsResultTable")
						.append('<tr><th scope="row">' + (i+1).toString() +
						        '</th><td>' + gotJson.recordsList[i].userCard + 
								'</td><td>' + gotJson.recordsList[i].date + 
								'</td><td>' + gotJson.recordsList[i].time + 
								'</td><td>' + toDisplayMoney(gotJson.recordsList[i].amount)+'</td><tr>');
					}
				},
				error:function(data){
					var gotJson = JSON.parse(data);
					console.log(gotJson);
					push_danger(gotJson.errorMsg);
				}
		});
}

function cleanQuery(){
	$("#querySalerRecordsResultPanel").css("display","none");
}

$(document).ready(function(){
	$("#saveState")[0].checked = true;
	if ($.cookie("saler_id") != "" && $.cookie("saler_activate_code") != "")
	if ($.cookie("saler_id") != null && $.cookie("saler_activate_code") != null){
		$("input[id=saler_id]").val( $.cookie("saler_id") );
		$("input[id=saler_activate_code]").val( $.cookie("saler_activate_code") );
		login();
	}	
})